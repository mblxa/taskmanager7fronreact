import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import LinksList from './navigation'
import {navigationLinksInitial as navLinks} from './reducers/NavigationLinks'


const App  =() => (
  <Router>
    <div>
      <LinksList/>
      <hr/>
      {navLinks.links.map(link =>
        <Route exact
               key={link.id}
               path={link.path}
               component={link.component}
        />
      )}

    </div>
  </Router>
)

export default App
