import { combineReducers } from 'redux'
import {User} from "./User";
import {navigationLinks} from "./NavigationLinks";
import {Tasks} from "./Tasks"


const taskManagerApp = combineReducers({
  User,
  navigationLinks,
  Tasks
})

export default taskManagerApp