import {REQUEST_TASKS, RECEIVE_TASKS, FETCH_TASKS} from "../actionTypes";


const tasksInitial = {
  tasks: []
}

export function Tasks(state = tasksInitial, action) {
  switch (action.type) {
    case FETCH_TASKS:
      return Object.assign({}, state, {})
    case RECEIVE_TASKS:
      return Object.assign({}, state, {
        tasks: action.tasks
      });
    default:
      return state
  }
}
