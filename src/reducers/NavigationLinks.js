import React from 'react'
import Auth from '../user/Auth'
import Dashboard from '../dashboard'

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
)

export const navigationLinksInitial = {
  links: [
    {id: 0, path: '/', text:'Home', component:Home, authorization: false},
    {id: 1, path: '/auth', text:'Auth', component:Auth, authorization: false},
    {id: 2, path: '/dashboard', text:'Dashboard', component:Dashboard, authorization: true},
  ]
};

export function navigationLinks(state = navigationLinksInitial, action) {
  return state
}
