import {USER_AUTH, USER_SAVE_TOKEN} from '../actionTypes'

const userAuthInitial = {
  token: null
}

export function User(state = userAuthInitial, action) {
  switch (action.type) {
    case USER_SAVE_TOKEN:
      return Object.assign({}, state, {
        token: action.token
      })
    case USER_AUTH:
      return Object.assign({}, state, {})
    default:
      return state
  }
}
