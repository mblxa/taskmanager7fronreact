import { connect } from 'react-redux'
import TaskList from './TaskList'
import {fetchTasks} from "../actionTypes";


const mapStateToProps = (state, ownProps) => ({
  tasks: state.Tasks.tasks,
  token: state.User.token
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchTasks: (token) => dispatch(fetchTasks(token))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskList)