import React from 'react'
import Task from './Task'
import { Redirect } from 'react-router'


class LinksList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {tasks: []}
    if (this.props.token) {
      this.props.fetchTasks(this.props.token)
    }
  }

  render() {
    if (!this.props.token) {
      return <Redirect to="/auth"/>
    }
    return (
      <div>
        <h2>Tasks</h2>
        <div className="row">
          {this.props.tasks.map(task =>
            <Task
              key={task.ID}
              task={task}
            />
          )}
        </div>
      </div>
    )
  }
}

export default LinksList