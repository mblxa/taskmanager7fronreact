import React from 'react'

const Task = ({task}) => (
  <div className="col-md-6">
  <div className="card">
    <div className="card-body">
      <h5 className="card-title">{task.Name}
        <a className="btn btn-sm btn-primary float-right complete">
          <i className="fa fa-calendar-check"></i>
        </a>
        <div className="dropdown dropleft float-right postpone">
          <button className="btn btn-sm btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i className="fa fa-clock"></i>
          </button>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a className="dropdown-item" >Postpone 10 minutes</a>
            <a className="dropdown-item" >Postpone 30 minutes</a>
            <a className="dropdown-item" >Postpone 1 hour</a>
            <a className="dropdown-item" >Postpone to tomorrow</a>
          </div>
        </div>
      </h5>
      <hr/>
      <h6 className="card-subtitle mb-2 text-muted">
        {task.Date}, {task.Duration} minutes
        <i className="fa fa-clock float-right btn btn-sm btn-info"> Soon</i>
        <i className="fa fa-clock float-right btn btn-sm btn-success"> In progress</i>
        <i className="fa fa-road float-right btn btn-sm btn-warning"> Ended</i>
      </h6>
      <p className="card-text">{task.Description}</p>
    </div>
  </div>
  </div>
)

export default Task