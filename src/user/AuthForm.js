import React from 'react'

const AuthForm = ({emailKeyup, passwordKeyup, submitForm}) => (
  <div className="row justify-content-center">
    <div className="col-md-4">
      <h3 className="text-center">Login</h3>
      <form onSubmit={submitForm}>
        <div className="form-group">
          <label htmlFor="Email">Email address</label>
          <input type="text" className="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email"
                 onChange={emailKeyup}
          />
          <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          <div className="invalid-feedback">
            Please enter email
          </div>
          <div className="invalid-feedback">
            Invalid email and/or password
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="Password">Password</label>
          <input type="password" className="form-control" id="Password" placeholder="Password"
                 onChange={passwordKeyup}
          />
          <div className="invalid-feedback">
            Please enter password
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-6"><button type="submit" className="btn btn-primary btn-block">Login</button></div>
        </div>
      </form>
    </div>
  </div>
)

export default AuthForm