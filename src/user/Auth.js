import React from 'react'
import {authorize} from '../actionTypes'
import AuthForm from './AuthForm'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

class Auth extends React.Component {
  constructor(props) {
    super(props)
    this.state = {email:'', password:'', authorized: false}
    this.updateEmail = this.updateEmail.bind(this);
    this.updatePassword = this.updatePassword.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(e) {
    e.preventDefault();
    this.props.authorize(this.state.email, this.state.password);
  }

  updateEmail(e) {
    this.setState({email: e.target.value})
  }
  updatePassword(e) {
    this.setState({password: e.target.value})
  }

  render() {
    if (this.props.authorized) {
      return <Redirect to="/dashboard"/>
    }

    return AuthForm({
      emailKeyup: this.updateEmail,
      passwordKeyup: this.updatePassword,
      submitForm: this.submitForm,
      authorized: this.props.authorized
    })
  }
}

const isAuthorized = (token) => {
  return token !== null
}

const mapStateToProps = (state) => ({
  authorized: isAuthorized(state.User.token)
})

const mapDispatchToProps = dispatch => ({
  authorize: (email, password) => dispatch(authorize(email, password))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth)