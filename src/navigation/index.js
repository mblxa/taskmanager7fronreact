import { connect } from 'react-redux'
import LinksList from './LinksList'


const getVisibleLinks = (state) => {
  if (state.User.token !== null) {
    return state.navigationLinks.links
  } else {
    return state.navigationLinks.links.filter(link => !link.authorization)
  }
}

const mapStateToProps = (state, ownProps) => ({
  links: getVisibleLinks(state)
})

const mapDispatchToProps = (dispatch, ownProps) => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LinksList)