import React from 'react'
import {
  Link
} from 'react-router-dom'

const LinkItem = ({ path, text}) => (
  <li><Link to={path}>{text}</Link></li>
)

export default LinkItem