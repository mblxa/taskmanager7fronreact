import React from 'react'
import LinkItem from './LinkItem'

const LinksList = ({ links }) => (
  <ul>
    {links.map(link =>
      <LinkItem
        key = {link.id}
        path = {link.path}
        text = {link.text}
      />
    )}
  </ul>
)

export default LinksList