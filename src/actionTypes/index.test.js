import {saveToken,authorize,USER_SAVE_TOKEN} from "./index";

describe('Actions', () => {
  it('should run saveToken', () => {
    const token = 'asda1231'
    let res = saveToken(token)
    expect(res.token).toBe(token)
    expect(res.type).toBe(USER_SAVE_TOKEN)
  });
})
