import axios from 'axios';

export const USER_SAVE_TOKEN = 'USER_SAVE_TOKEN'
export const USER_AUTH = 'USER_AUTH'

export function saveToken(token) {
  return {type: USER_SAVE_TOKEN, token}
}

export function authorize(email, password) {
  return dispatch => {
    const body = JSON.stringify({email: email, password: password})
    return axios.post(`https://managetasks.ru:5003/api/v1/user/auth`, body)
      .then(function (resp) {
        if (resp.status === 200) {
          dispatch(saveToken(resp.data.token))
        }
      })
  }

  // return {type: USER_AUTH, credentials: {email: email, password: password}}
}


export const RECEIVE_TASKS = 'RECEIVE_TASKS'
export const FETCH_TASKS = 'FETCH_TASKS'

function receiveTasks(tasks) {
  return {
    type: RECEIVE_TASKS,
    tasks: tasks,
  }
}

export function fetchTasks(token) {
  return dispatch => {
    return axios.get(`https://managetasks.ru:5003/api/v1/tasks`, {headers: {'token': token}})
      .then(data => {
        dispatch(receiveTasks(data.data))
      })
  }
}